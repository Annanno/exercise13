package com.example.exercise13

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.exercise13.model.ModelItem
import com.example.exercise13.network.Resource
import com.example.exercise13.network.RetrofitInstance
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response

class ResponseViewModel : ViewModel() {

    var response: MutableLiveData<Resource<Response<MutableList<ModelItem>>>> = MutableLiveData()
    fun getData() {

        viewModelScope.launch {
//            delay(4000)
            withContext(Dispatchers.IO) {
                response.postValue(Resource.Loading(true))
                try {
                    val result = RetrofitInstance.retrofit.getData()
                    if (result.isSuccessful && result.body()!= null)
                        response.postValue(Resource.Success(result))
                    else
                        response.postValue(Resource.Error(result.message()))

                } catch (e: Exception) {
                    response.postValue(Resource.Error(e.message))
                }
                response.postValue(Resource.Loading(false))

            }

        }

    }

}