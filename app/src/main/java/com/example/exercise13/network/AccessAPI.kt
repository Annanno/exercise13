package com.example.exercise13.network

import com.example.exercise13.model.ModelItem
import retrofit2.Response
import retrofit2.http.GET

interface AccessAPI {

    @GET("v3/c111ca66-46e7-400e-ab5d-809865408c66")
    suspend fun getData(): Response<MutableList<ModelItem>>

}