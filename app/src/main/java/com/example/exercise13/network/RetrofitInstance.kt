package com.example.exercise13.network

import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object RetrofitInstance {

    val retrofit: AccessAPI by lazy {
        Retrofit.Builder()
            .baseUrl("https://run.mocky.io")
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
            .create(AccessAPI::class.java)
    }
}