package com.example.exercise13.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.exercise13.databinding.ItemLayoutBinding
import com.example.exercise13.extensions.setImage
import com.example.exercise13.model.ModelItem

class ModelAdapter : RecyclerView.Adapter<ModelAdapter.ViewHolder>() {

    var list: MutableList<ModelItem> = mutableListOf()


    inner class ViewHolder(private val binding: ItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind() {
            binding.title.text = list[adapterPosition].title
            binding.publisherData.text = list[adapterPosition].date
            binding.image.setImage(list[adapterPosition].imgUrl)


        }

    }

    override fun getItemCount() = list.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()

    }

    fun setData(newList: MutableList<ModelItem>) {
        this.list.clear()
        this.list.addAll(newList)
        notifyDataSetChanged()
    }
}