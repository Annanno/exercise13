package com.example.exercise13.fragments


import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.exercise13.ResponseViewModel
import com.example.exercise13.adapter.ModelAdapter
import com.example.exercise13.databinding.FragmentMainBinding
import com.example.exercise13.network.Resource

class MainFragment : BaseFragment<FragmentMainBinding>(FragmentMainBinding::inflate) {

    private lateinit var modelAdapter: ModelAdapter
    private val viewModel: ResponseViewModel by viewModels()

    override fun init() {
        viewModel.getData()
        initRecyclerView()
        observe()
    }

    private fun initRecyclerView() {
        binding.recycler.layoutManager = LinearLayoutManager(requireContext())
        modelAdapter = ModelAdapter()
        binding.recycler.adapter = modelAdapter
    }

    private fun observe() {
        viewModel.response.observe(viewLifecycleOwner, {
            when (it) {
                is Resource.Success -> {
                    modelAdapter.setData(it.data?.body()!!)
                }
                is Resource.Error -> {
                    Toast.makeText(context, "${it.message}", Toast.LENGTH_SHORT).show()
                }
                is Resource.Loading -> {
                    binding.progressBar.isVisible = it.isLoading
                }
            }
        })
    }


}